package ru.itis.formulas.utils

import ru.itis.formulas.model.Planet

enum class SolarSystemPlanets(val planet: Planet) {
    EARTH(
        Planet(
            radius = 6378.14,
            mass = 5.972e24,
            atmosphereDensity = 1.2754,
            gravitationalAcceleration = 9.8
        )
    ),
    MERCURY(
        Planet(
            radius = 2439.0,
            mass = 3.285e23,
            atmosphereDensity = 0.0,
            gravitationalAcceleration = 3.7
        )
    ),
    VENUS(
        Planet(
            radius = 6051.5,
            mass = 4.867e24,
            atmosphereDensity = 67.0,
            gravitationalAcceleration = 8.87
        )
    ),
    MARS(
        Planet(
            radius = 3397.4,
            mass = 6.39e23,
            atmosphereDensity = 0.020,
            gravitationalAcceleration = 3.721
        )
    ),
    JUPITER(
        Planet(
            radius = 69911.0,
            mass = 1.898e27,
            atmosphereDensity = 0.00,
            gravitationalAcceleration = 24.8
        )
    ),
    SATURN(
        Planet(
            radius = 58232.0,
            mass = 5.683e26,
            atmosphereDensity = 0.19,
            gravitationalAcceleration = 10.4
        )
    ),
    URANUS(
        Planet(
            radius = 25362.0,
            mass = 8.681e25,
            atmosphereDensity = 0.42,
            gravitationalAcceleration = 8.87
        )
    ),
    NEPTUNE(
        Planet(
            radius = 24622.0,
            mass = 1.024e26,
            atmosphereDensity = 0.45,
            gravitationalAcceleration = 10.15
        )
    ),
    PLUTO(
        Planet(
            radius = 1195.0,
            mass = 1.303e22,
            atmosphereDensity = 0.0,
            gravitationalAcceleration = 0.66
        )
    ),
    MOON(
        Planet(
            radius = 2736.9,
            mass = 7.342e22,
            atmosphereDensity = 0.0,
            gravitationalAcceleration = 1.62
        )
    ),
    SUN(
        Planet(
            radius = 695500.0,
            mass = 2e30,
            atmosphereDensity = 0.0,
            gravitationalAcceleration = 274.0
        )
    )
}
